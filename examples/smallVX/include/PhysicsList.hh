#ifndef PhysicsList_h
#define PhysicsList_h_1
#include "globals.hh"
#include "G4VModularPhysicsList.hh"

#include "G4VModularPhysicsList.hh"
#include "G4EmConfigurator.hh"
#include "globals.hh"

class G4VPhysicsConstructor;

class myPhysicsList: public G4VModularPhysicsList
{
public:
  myPhysicsList();
  virtual ~myPhysicsList();

  void ConstructParticle();
  void ConstructProcess();
  void SetCuts();

private:
  G4VPhysicsConstructor*               emPhysicsList;
  std::vector<G4VPhysicsConstructor*>  hadronPhys;
};

#endif
