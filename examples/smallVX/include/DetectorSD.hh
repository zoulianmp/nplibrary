#ifndef DetectorSDDet
#define DetectorSDDet

#include "G4VSensitiveDetector.hh"
#include <map>
#include <vector>
#include "NPlayerData.hh"


class G4Step;
class RunAction;

class DetectorSD: public G4VSensitiveDetector 
{
	public:
    DetectorSD(G4String, G4int, G4int, G4int);
    ~DetectorSD();
		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
		void EndOfEvent(G4HCofThisEvent*);
private:
  G4String ownName;
  RunAction* runAction;  // @todo: reparse to Run
  G4int curEventNo;
  G4bool curEventParsed;

  G4int fnX, fnY, fnZ;
};

#endif