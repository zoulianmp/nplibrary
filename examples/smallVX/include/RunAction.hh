#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"
#include "NPlayerData.hh"
#include "Run.hh"
#include <map>
#include <vector>
#include <ctime>
#include "G4VProcess.hh"

#include "g4root.hh"
//#include "TROOT.h"
//#include "TFile.h"
//#include "TNtuple.h"

//#define USE_STEPPING_COUNTER 1

#ifdef USE_STEPPING_COUNTER
#endif

static int64_t millisSinceEpoch();
void __GetMachineName(char* machineName);

class G4Run;
class Run;
class DetectorConstruction;
class RunAction : public G4UserRunAction
{
  public:
    RunAction(DetectorConstruction*);  // dummy constructor
    virtual ~RunAction();

    virtual void BeginOfRunAction(const G4Run* run);
    virtual void   EndOfRunAction(const G4Run* run);

    void StoreDB(G4int);
    void dumpRootData();
    bool rootDumped;

    inline G4double getDose() {return ene;};

    void printDose();


public:
    DetectorConstruction* fDetector;
    bool dumpNeeded();
    void dumpData();
    void closeRootData();

    void myEndRun(G4int);

    //TNtuple *theROOTNtuple;

    G4Run* GenerateRun();
  private:

    int64_t startedAtms;
    int64_t endedAtms;

    Run* fRun;

    G4double ene;
    G4double ene2;
    G4int cnt;

    bool useContinue;
    G4String binaryFileName;
    G4String additionalFileName;

	  clock_t beginAt;
    clock_t dumpedAt;
	  clock_t finishedAt1;
    clock_t finishedAtFull;
    G4int ctme;

    G4int sumNoE;
    G4int currentRunNo;

    void initAnalysis();

    resLayer myRes[3];

  //std::map<std::string, G4int> *mParticle2Spectrum;

  //std::vector<G4int> npsSum;


};

#endif