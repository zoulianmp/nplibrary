#include "DetectorConstruction.hh"

#include "DetectorSD.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4Sphere.hh"
#include "globals.hh"
#include "G4VisAttributes.hh" 
#include "G4SDManager.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4PVReplica.hh"
#include "G4UImessenger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4RunManager.hh"
#include "G4UIcmdWithABool.hh"
#include "G4Tubs.hh"
#include "NPParamReader.hh"
#include "boost/lexical_cast.hpp"
#include "G4Cons.hh"
#include "G4Trd.hh"
#include "G4Sphere.hh"
#include "G4GenericTrap.hh"
#include "G4TwoVector.hh"
#include "G4TransportationManager.hh"
#include "G4SubtractionSolid.hh"
#include "G4EllipticalTube.hh"
#include "G4AssemblyVolume.hh"
#include "G4Transform3D.hh"
#include "PhantomParameterisation.hh"
#include "G4PVParameterised.hh"
#include "NPExtra.hh"

#include <map>

DetectorConstruction::DetectorConstruction(): G4VUserDetectorConstruction(),
  activeBoxLVisSet(false)
{
}

DetectorConstruction::~DetectorConstruction() {}

// ������ ��� ����������� ������ �������
#define SIZE(x) sizeof(x)/sizeof(*x)

void DetectorConstruction::constructMaterials() {
  G4NistManager* nistMan = G4NistManager::Instance(); 
  G4Element *elC = nistMan->FindOrBuildElement("C");
  G4Element *elH = nistMan->FindOrBuildElement("H");
  G4Element *elLi = nistMan->FindOrBuildElement("Li");
  G4Element *elN = nistMan->FindOrBuildElement("N");
  G4Element *elK = nistMan->FindOrBuildElement("K");
  G4Element *elO = nistMan->FindOrBuildElement("O");
  G4Element *elNa = nistMan->FindOrBuildElement("Na");
  G4Element *elS = nistMan->FindOrBuildElement("S");
  G4Element *elCl = nistMan->FindOrBuildElement("Cl");
  G4Element *elBr = nistMan->FindOrBuildElement("Br");

  G4Material *SiO2 = nistMan->FindOrBuildMaterial("G4_SILICON_DIOXIDE");

  G4Material* EBT3_ActiveV2 = new G4Material("EBT3_activeLayer", 1.2*g/cm3, 8);
  EBT3_ActiveV2->AddElement(elO, 0.2870);
  EBT3_ActiveV2->AddElement(elS, 0.0054);
  EBT3_ActiveV2->AddElement(elH, 0.0983);
  EBT3_ActiveV2->AddElement(elCl, 0.0059);
  EBT3_ActiveV2->AddElement(elC, 0.5879);
  EBT3_ActiveV2->AddElement(elNa, 0.0039);
  EBT3_ActiveV2->AddElement(elN, 0.0024);
  EBT3_ActiveV2->AddElement(elLi, 0.0093);

  G4Material* EBT3_PETV2 = new G4Material("EBT3_PETLayer", 1.35*g/cm3, 3);
  EBT3_PETV2->AddElement(elH, 0.0420);
  EBT3_PETV2->AddElement(elC, 0.6250);
  EBT3_PETV2->AddElement(elO, 0.3330);

  G4Material *PETSiOLayerV2 = new G4Material("EBT3_PETSiOLayer", 
    1.35*g/cm3,
    2);

  PETSiOLayerV2->AddMaterial(EBT3_PETV2, 0.99986);
  PETSiOLayerV2->AddMaterial(SiO2, 0.00014);

  G4double z, a, density;
  new G4Material("Carbon"     , z=6.,  a= 12.01*g/mole, density= 2.267*g/cm3);

  G4Material* EBT2_Sum = new G4Material("EBT2_Sum", 1.2500*g/cm3, 8);
  EBT2_Sum->AddElement(elO, 0.3236);
  EBT2_Sum->AddElement(elK, 0.0005);
  EBT2_Sum->AddElement(elH, 0.0502);
  EBT2_Sum->AddElement(elC, 0.6204);
  EBT2_Sum->AddElement(elCl, 0.0017);
  EBT2_Sum->AddElement(elN, 0.0017);
  EBT2_Sum->AddElement(elLi, 0.0008);
  EBT2_Sum->AddElement(elBr, 0.0010);

  G4Material* c8h8 = new G4Material("c8h8", 1.04*g/cm3, 2);
  c8h8 ->AddElement(elC, 0.923);
  c8h8 ->AddElement(elH, 0.077);

  G4Material* c4h6 = new G4Material("c4h6", 1.04*g/cm3, 2);
  c4h6 ->AddElement(elC, 0.888);
  c4h6 ->AddElement(elH, 0.112);

  G4Material* c3h3n = new G4Material("c3h3n", 1.04*g/cm3, 3);
  c3h3n ->AddElement(elC, 0.8);
  c3h3n ->AddElement(elH, 0.066);
  c3h3n ->AddElement(elN, 0.134);

  G4Material *absPlast1 = new G4Material("absPlast1", 1.04*g/cm3, 3);
  absPlast1->AddMaterial(c8h8, 0.5);
  absPlast1->AddMaterial(c4h6, 0.25);
  absPlast1->AddMaterial(c3h3n, 0.25);

  G4Material *penoplex = new G4Material("penoplex1", 0.03758*g/cm3, 2);
  penoplex->AddElement(elH, 8);
  penoplex->AddElement(elC, 8);
}


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  myParamReader &paramReader = myParamReader::GetInstance();
  G4NistManager* nistMan = G4NistManager::Instance(); 
  G4Material *Galactic = nistMan->FindOrBuildMaterial("G4_Galactic");
  G4Material *air = nistMan->FindOrBuildMaterial("G4_AIR");
  G4Material *water = nistMan->FindOrBuildMaterial("G4_WATER");
  G4Material *polycarbonate = nistMan->FindOrBuildMaterial("G4_POLYCARBONATE");
  G4Material *plexiglass = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
  G4Material *Si = nistMan->FindOrBuildMaterial("G4_Si");
  constructMaterials();
  G4double world_x_length = 5*cm;
  G4double world_y_length = 5*cm;
  G4double world_z_length = 34*cm;

  G4Box* world_box = new G4Box("world", 0.5*world_x_length, 0.5*world_y_length, 0.5*world_z_length);
  G4LogicalVolume* world_log = new G4LogicalVolume(world_box, air, "world");
  G4VPhysicalVolume* world_phys = new G4PVPlacement(0, G4ThreeVector(), world_log, "world", 0, false, 0);

  mDBSingleton *mData = mDBSingleton::GetInstance();
  mData->addMetaData("vxX", 250);
  mData->addMetaData("vxY", 250);
  mData->addMetaData("vxZ", 320);
  createPhantom(world_log, G4ThreeVector(0, 0, 0), new G4RotationMatrix());

  return world_phys;
}

void DetectorConstruction::createPhantom(G4LogicalVolume *parentLV, G4ThreeVector topPos, G4RotationMatrix* topRot) {
  myParamReader &paramReader = myParamReader::GetInstance();
  G4NistManager* nistMan = G4NistManager::Instance(); 
  G4Material *dummy = nistMan->FindOrBuildMaterial("G4_AIR");
  G4double topSizeX = 4*cm;
  G4double topSizeY = 4*cm;
  G4double topSizeZ = 32*cm;

  G4Box *topPhantomBox = new G4Box("topPhantomBox", 0.5*topSizeX, 0.5*topSizeY, 0.5*topSizeZ);
  G4LogicalVolume *topPhantomBoxLV = new G4LogicalVolume(topPhantomBox, dummy, "topPhantomBoxLV");
  G4VPhysicalVolume *topPhantomBoxPV = new G4PVPlacement(topRot, topPos, topPhantomBoxLV, "topPhantomBoxPV", parentLV, false, 0);

  mDBSingleton *mData = mDBSingleton::GetInstance();
  G4int vxX = mData->getS2IMeta("vxX");
  G4int vxY = mData->getS2IMeta("vxY");
  G4int vxZ = mData->getS2IMeta("vxZ");

  G4Box *phantomBoxX = new G4Box("phantomBoxX", 0.5*topSizeX / vxX, 0.5*topSizeY, 0.5*topSizeZ);
  G4LogicalVolume *phantomBoxXLV = new G4LogicalVolume(phantomBoxX, dummy, "phantomBoxXLV");
  G4VPhysicalVolume *phantomBoxXPV = new G4PVReplica("phantomBoxXRep", phantomBoxXLV, topPhantomBoxLV, kXAxis, vxX, phantomBoxX->GetXHalfLength()*2, 0.0);

  G4Box *phantomBoxY = new G4Box("phantomBoxY", 0.5*topSizeX / vxX, 0.5*topSizeY / vxY, 0.5*topSizeZ);
  G4LogicalVolume *phantomBoxYLV = new G4LogicalVolume(phantomBoxY, dummy, "phantomBoxYLV");
  G4VPhysicalVolume *phantomBoxYPV = new G4PVReplica("phantomBoxYRep", phantomBoxYLV, phantomBoxXLV, kYAxis, vxY, phantomBoxY->GetYHalfLength()*2, 0.0);

  G4Box *phantomBoxZ = new G4Box("phantomBoxY", 0.5*topSizeX / vxX, 0.5*topSizeY / vxY, 0.5*topSizeZ / vxZ);
  G4LogicalVolume *phantomBoxZLV = new G4LogicalVolume(phantomBoxZ, dummy, "phantomBoxZLV");

  G4ThreeVector voxelSize;
  voxelSize.setX(phantomBoxX->GetXHalfLength()*2);
  voxelSize.setY(phantomBoxY->GetYHalfLength()*2);
  voxelSize.setZ(phantomBoxZ->GetZHalfLength()*2);
  G4ThreeVector blCorner;
  blCorner.setX(topPhantomBox->GetXHalfLength());
  blCorner.setY(topPhantomBox->GetYHalfLength());
  blCorner.setZ(topPhantomBox->GetZHalfLength());
  PhantomParameterisation* param = new PhantomParameterisation(voxelSize, blCorner, vxX, vxY, vxZ);

  G4VPhysicalVolume *phantomBoxZPV = new G4PVParameterised("phantomParametrised",    // their name
    phantomBoxZLV, // their logical volume
    phantomBoxYLV,      // Mother logical volume
//    topPhantomBoxLV,
    kZAxis,       // Are placed along this axis
    //kUndefined,
                          // Are placed along this axis
    vxZ,      // Number of cells
    param);       // Parameterisation.

  ActiveBoxLV = phantomBoxZLV;
  activeBoxLVisSet = true;

  vxMain *vxData = vxMain::Instance();
  vxData->setVoxelData(vxX, vxY, vxZ, voxelSize); // @todo: bypass LV if needed?

  G4double baseCut = boost::lexical_cast<G4double>(paramReader.getOtherParamAsString("baseCut", "700"));
  baseCut *= micrometer;
  G4Region *regExpPipe;
	regExpPipe = new G4Region("experiment");
  phantomBoxZLV->SetRegion(regExpPipe);
  topPhantomBoxLV->SetRegion(regExpPipe);
  G4ProductionCuts* pCut = new G4ProductionCuts();
  pCut->SetProductionCut(0*mm, "proton");
  pCut->SetProductionCut(baseCut, "gamma");
  pCut->SetProductionCut(baseCut, "e+");
  pCut->SetProductionCut(baseCut, "e-");
  regExpPipe->SetProductionCuts(pCut);
  regExpPipe->AddRootLogicalVolume(topPhantomBoxLV);
}



void DetectorConstruction::ConstructSDandField() {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  sdMan->SetVerboseLevel(1);
  vxMain *vxData = vxMain::Instance();
  G4double gtDeDx;
  G4int procType;
  myParamReader &paramReader = myParamReader::GetInstance();
  if (paramReader.getOtherParamAsString("--cont", "0") == "0") {
    // case 1st run
    vxData->addFilter(20*keV/um, filterType::npF_gtDeDx);
    vxData->addFilter(50*keV/um, filterType::npF_gtDeDx);
    vxData->addFilter(100*keV/um, filterType::npF_gtDeDx);
    vxData->addFilter(2, filterType::npF_procType);   // ionisation
    vxData->addFilter(111, filterType::npF_procType); // hadElastic
    vxData->addFilter(121, filterType::npF_procType); // inelastic
  }
  mDBSingleton *mData = mDBSingleton::GetInstance();
  DetectorSD *mVxDet = new DetectorSD("vxl", mData->getS2IMeta("vxX"), mData->getS2IMeta("vxY"), mData->getS2IMeta("vxZ"));
  ActiveBoxLV->SetSensitiveDetector(mVxDet);
  sdMan->SetVerboseLevel(0);
}

