#include "RunAction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
//#include "G4VUserDetectorConstruction.hh"
#include "DetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "DetectorSD.hh"
#include "NPlayerData.hh"
#include <stdio.h>  /* defines FILENAME_MAX */
#ifdef _WIN32
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
#endif
#include <ctime>
#include "G4HadronicProcessStore.hh"
#include "G4VProcess.hh"
#include "G4LossTableManager.hh"
#include "G4ProductionCutsTable.hh"
#include "NPparamReader.hh"
#include "Run.hh"
#include "NPExtra.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "globals.hh"
#include "DetectorConstruction.hh"

//#define NO_GZIP 1

#define NO_RA

//#define USE_STEPPING_COUNTER 1

#ifdef USE_STEPPING_COUNTER
#endif

#include "g4root.hh"
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <windows.h>
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

#include "mongo/client/dbclient.h" // for the driver
#include "mongo/bson/bson.h"
#include "mongo/bson/bsonobj.h"
#include "mongo/bson/bsonobjbuilder.h"
#include <time.h>
#include <chrono>
#include "boost/date_time/posix_time/posix_time.hpp"
// @see: http://stackoverflow.com/questions/10973846/convert-between-boostposix-timeptime-and-mongodate-t

//#include "Run.hh"



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#define DUMP_AFTER_SECONDS 300

RunAction::RunAction(DetectorConstruction* fdet) : G4UserRunAction(), fDetector(fdet)
{
  //mParticle2Spectrum = new std::map<std::string, G4int>[1000];
  vxMain *vxData = vxMain::Instance();
  myParamReader &paramReader = myParamReader::GetInstance();
  if (paramReader.getOtherParamAsString("--cont", "0") == "1") {
    G4String pts = paramReader.getOtherParamAsString("--binaryFileName", "test.root");
    vxData->readDump(pts+G4String("_temp.bin"));
  }
}


RunAction::~RunAction()
{

}

G4Run* RunAction::GenerateRun()
{ 
  fRun = new Run(fDetector);
  return fRun;
}

static int64_t millisSinceEpoch()
{
  return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

void __GetMachineName(char* machineName)
{
    char Name[150];
    int i=0;

    #ifdef WIN32
        TCHAR infoBuf[150];
        DWORD bufCharCount = 150;
        memset(Name, 0, 150);
        if( GetComputerName( infoBuf, &bufCharCount ) )
        {
            for(i=0; i<150; i++)
            {
                Name[i] = infoBuf[i];
            }
        }
        else
        {
            strcpy(Name, "Unknown_Host_Name");
        }
    #else
        memset(Name, 0, 150);
        gethostname(Name, 150);
    #endif
    strncpy(machineName,Name, 150);
}

void RunAction::BeginOfRunAction(const G4Run* cRun)
{ 
  G4RunManager::GetRunManager()->SetPrintProgress(1000000);


  myParamReader &paramReader = myParamReader::GetInstance();
  mDBSingleton *mData = mDBSingleton::GetInstance();

  beginAt = dumpedAt = clock();

  startedAtms = millisSinceEpoch();
  G4cout<<"Started at (ms): "<<startedAtms<<G4endl;

  initAnalysis();
}



bool RunAction::dumpNeeded() {
  if ( (clock() - dumpedAt) / CLOCKS_PER_SEC > 600) {
    return true;
  }
  return false;
}

//#define USE_VOXEL_METHOD 1

void RunAction::initAnalysis() {
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->SetVerboseLevel(1);
  //binaryFileName.copy();
  //G4String analysisFileName = G4String(binaryFileName).replace(4, 1, "l");
  myParamReader &paramReader = myParamReader::GetInstance();
  G4String analysisFileName = paramReader.getOtherParamAsString("--binaryFileName", "test.root");
  analysisManager->SetFileName(analysisFileName);

  //analysisManager->CreateH1("targetDeDxSpectrum", "target dE/dX Full", 1000, 0, 500); // 0
  //analysisManager->CreateH1("targetDeDxSpectrumPrimaries", "target dE/dX Primaries", 1000, 0, 500); // 1

  //analysisManager->CreateH1("targetKinetics", "target kinetics full", 1000, 0*GeV, 1*GeV, "MeV"); // 2
  //analysisManager->CreateH1("targetKineticsPrimaries", "target kinetics primaries", 1000, 0*GeV, 1*GeV, "MeV"); // 3

  //analysisManager->CreateH1("spectrumBigBoxEntriesPrimaries", "Spectrum at big box entry, primaries", 1000, 0*GeV, 5*GeV); // 4
  //analysisManager->CreateH1("spectrumKessonEntriesPrimaries", "Spectrum at kesson entry, primaries", 1000, 0*GeV, 5*GeV); // 5


#ifdef USE_VOXEL_METHOD
  //aaa
  analysisManager->CreateH2("ebtDoseMap", "EBT along dose map", 401, 0, 400, 401, 0, 400); // H2 0
  //analysisManager->CreateH1("allDep", 
  analysisManager->CreateH1("dedxspectrum", "dedx at ebt3 along", 900, 0, 900); // H1 0

#else
  
#endif

  analysisManager->OpenFile();
}

void RunAction::printDose() {

}

void RunAction::dumpData() {
  //printDose();
  dumpRootData();
  G4cout<<"Dumped after "<<(clock() - beginAt) / CLOCKS_PER_SEC<<" seconds. NPS = "
    <<G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEvent()<<G4endl;
  dumpedAt = clock();
  
}

void RunAction::myEndRun(G4int isInterrupted = 0) {
  if (isMaster) fRun->EndOfRun();    

  //printDose();
  dumpRootData();
  dumpedAt = clock();
  StoreDB(isInterrupted);
  closeRootData();
}

void RunAction::StoreDB(G4int isInterrupted = 0) {

  myParamReader &paramReader = myParamReader::GetInstance();
  G4String isDbStore = paramReader.getOtherParamAsString("--noDBStore", "0");  // @todo: just debug case
  if (isDbStore == "0") {
    return;
  }
  if (paramReader.isUseUI()) {
    // don't store visualization case
    return;
  }

    mongo::client::initialize();
    mongo::DBClientConnection c; // @todo: use global
    try {
      c.connect("192.168.1.215");
    } catch( const mongo::DBException &e ) {
        std::cout << "caught " << e.what() << std::endl;
    }
    endedAtms = millisSinceEpoch();
    G4cout<<"Ended at (ms): "<<endedAtms<<G4endl;

    // exe full path
    TCHAR szExeFileName[MAX_PATH]; 
    GetModuleFileName(NULL, szExeFileName, MAX_PATH);
    //std::cout<<szExeFileName<<std::endl;

    // machine name
    char machineName[150];
    __GetMachineName(machineName);

    char cCurrentPath[FILENAME_MAX];
    if (!GetCurrentDir(cCurrentPath, sizeof(cCurrentPath))) {}
    cCurrentPath[sizeof(cCurrentPath) - 1] = '\0';

    G4String rootOutputFullPath = G4String(cCurrentPath)+"\\"+paramReader.getOtherParamAsString("--binaryFileName", "test.root");
    G4String macInputPath = G4String(cCurrentPath)+"\\"+paramReader.getCmd();

    G4String dff = paramReader.getOtherParamAsString("--dShift", "0"); // @todo: refactor

    mongo::BSONObjBuilder b;
    b.append("name", "smallvox test");
    b.append("comments", "Simple voxels test");
    b.append("startDateTime", mongo::Date_t(startedAtms));
    b.append("endDateTime", mongo::Date_t(endedAtms));
    b.append("isInterrupted", isInterrupted);
    b.append("fullExePath", szExeFileName);
    b.append("rootOutputPath", rootOutputFullPath); // @todo
    b.append("originFolderPath", cCurrentPath); // @todo: getcwd/getcwd_
    b.append("macFilePath", macInputPath); // @todo: macfile
    b.append("physicsBaseModifier", paramReader.getPhysicsModifierValue(true));
    b.append("emPhysicsModifier", "");
    b.append("cascadePhysicsModifier", "");
    b.append("machineName", machineName);
    b.append("runModifiers", "");
    b.append("detectorConstructionStringParams", fDetector->filterParamString);
    b.append("eventActionStringParams", "");
    b.append("steppingActionStringParams", "");
    b.append("runActionStringParams", "");

    mongo::BSONObjBuilder bExtra;
    bExtra.append("title", "test1");
    mongo::BSONObj pExtra = bExtra.obj();
    b.append("extraData", pExtra);

    mongo::BSONObj p = b.obj();
    p.dump();

    c.insert("npplan3.geant4projects", p);
}

void RunAction::EndOfRunAction(const G4Run* cRun)
{
  vxMain *vxData = vxMain::Instance();
  vxData->finalize();
  myEndRun(0);
  //vxMain *vxData = vxMain::Instance();
  myParamReader &paramReader = myParamReader::GetInstance();
  G4String pts = paramReader.getOtherParamAsString("--binaryFileName", "test.root");
  if (isMaster) {
    vxData->dump(pts+G4String("_temp.bin"));
  }
}

void RunAction::dumpRootData() {
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  /*for (std::map<G4int, detLayerStepWeighted>::iterator it = rData.begin(); it != rData.end(); it++) {
    analysisManager->FillH1(0, it->first, it->second.eDep);
  }*/
  //analysisManager->CloseFile();
  //analysisManager->OpenFile();
  analysisManager->Write();
  
  /*if (!rootDumped) {
    analysisManager->Write();
    analysisManager->CloseFile();
    rootDumped = true;
  }*/
}

void RunAction::closeRootData() {
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->CloseFile();

  G4cout<<"Total NPS: "<<G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEvent();
  G4cout<<" Total time: "<<(clock() - beginAt) / CLOCKS_PER_SEC<<G4endl;
}
