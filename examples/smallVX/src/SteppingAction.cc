//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file hadronic/Hadr03/src/SteppingAction.cc
/// \brief Implementation of the SteppingAction class
//
// $Id: SteppingAction.cc 73011 2013-08-15 08:48:30Z gcosmo $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "SteppingAction.hh"
#include "RunAction.hh"
//#include "HistoManager.hh"

#include "G4ParticleTypes.hh"
#include "G4RunManager.hh"
#include "G4HadronicProcess.hh"
#include "EventAction.hh"
#include "G4SystemOfUnits.hh"
#include "Run.hh"
#include "NPParamReader.hh"
                           
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(RunAction* RuAct, EventAction* EvtAct)
: G4UserSteppingAction(),fRunAction(RuAct), cEventAction(EvtAct)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{ }

void SteppingAction::addData2Histo(const G4Step* aStep, G4String num) {
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  if (aStep->GetTrack()->GetParticleDefinition() == G4Neutron::NeutronDefinition()) {
      G4int iiD = analysisManager->GetH1Id(G4String("neutronSpectrum")+num, false);
      analysisManager->FillH1(iiD, aStep->GetTrack()->GetKineticEnergy());
  } else if ((aStep->GetTrack()->GetParticleDefinition() == G4Electron::ElectronDefinition()) || (aStep->GetTrack()->GetParticleDefinition() == G4Positron::PositronDefinition())) {
      G4int iiD = analysisManager->GetH1Id(G4String("e-e+Spectrum")+num, false);
      analysisManager->FillH1(iiD, aStep->GetTrack()->GetKineticEnergy());
  } else {
    if ((aStep->GetTrack()->GetParticleDefinition()->GetAtomicMass() >= 1) && (aStep->GetTrack()->GetParticleDefinition()->GetAtomicNumber() >= 1)) {
      G4int iiD = analysisManager->GetH1Id(G4String("othSpectrum")+num, false);
      analysisManager->FillH1(iiD, aStep->GetTrack()->GetKineticEnergy());
      iiD = analysisManager->GetH1Id(G4String("oth2Spectrum")+num, false);
      analysisManager->FillH1(iiD, aStep->GetTrack()->GetKineticEnergy()/aStep->GetTrack()->GetParticleDefinition()->GetAtomicMass());
    }
  }
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* aStep)
{
  const G4StepPoint* endPoint = aStep->GetPostStepPoint();
  const G4StepPoint* startPoint = aStep->GetPreStepPoint();
  G4StepStatus stepStatus = endPoint->GetStepStatus();

  G4String volName; 
  G4Track* track = aStep->GetTrack();
  if (track->GetVolume()) volName =  track->GetVolume()->GetName(); 
  G4String nextVolName;
  if (track->GetNextVolume()) nextVolName =  track->GetNextVolume()->GetName();  
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();


  G4bool transmit = (stepStatus==fGeomBoundary || stepStatus==fWorldBoundary);
  if (transmit) return;

  Run* run = static_cast<Run*>(
        G4RunManager::GetRunManager()->GetNonConstCurrentRun());
  run->addF2Analog(aStep->GetTrack()->GetKineticEnergy(), aStep->GetStepLength(), endPoint->GetPhysicalVolume()->GetName());
  //if (volName == "topPassDetExternPV" || volName == "endInternPV") {
    if (10 == aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessSubType()) {
      // msc process, need to be handled differently
      run->addProcessCounter(aStep->GetTrack()->GetParticleDefinition()->GetParticleName()+aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName(), aStep->GetTotalEnergyDeposit());
    } else {
      run->addProcessCounter(aStep->GetTrack()->GetParticleDefinition()->GetParticleName()+aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName(), aStep->GetTotalEnergyDeposit());
      run->addProcessCounter(aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName(), aStep->GetTotalEnergyDeposit());
    }
    if(0 == aStep->GetTrack()->GetParentID()) {
      run->addProcessCounter("prim", aStep->GetTotalEnergyDeposit());
      run->addProcessCounter(G4String("prim")+aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName(), aStep->GetTotalEnergyDeposit());
    }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


