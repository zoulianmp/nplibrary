#include "EventAction.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4UnitsTable.hh"
#include "RunAction.hh"

EventAction::EventAction(RunAction* RuAct)
 : G4UserEventAction(), cRunAction(RuAct)
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction()
{

}

void EventAction::BeginOfEventAction(const G4Event* aEvent)
{

}

void EventAction::EndOfEventAction(const G4Event* event)
{  
  /*if (cRunAction->dumpNeeded()) {
    cRunAction->dumpData();

    // ROOT data duplicated in this case
    // @todo: improve
  }*/
}
