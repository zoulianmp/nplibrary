/*
 * Base file for geant projects
 * Main program definition
 * @author: mrxak
 * (c) MRRC, 2013
*/

//#define IS_EXP 1

#include "globals.hh"
#include "DetectorConstruction.hh"
#include "ActionInitialization.hh"
//#include "Randomize.h"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "NPParamReader.hh"

#include "RunAction.hh"

#include "G4UImanager.hh"
#include "G4VModularPhysicsList.hh"
#include "PhysicsList.hh"
#include "G4StepLimiterPhysics.hh"
#include "FTFP_BERT.hh"
#include "QGSP_BIC.hh"
#include "QGSP_BIC_HP.hh"
#include "FTFP_BERT_HP.hh"
#include "QGSP_INCLXX.hh"
#include "QGSP_INCLXX_HP.hh"
#include "FTFP_INCLXX.hh"
#include "FTFP_INCLXX_HP.hh"
#include "QBBC.hh"
#include "Shielding.hh"
#include "G4GenericIon.hh"
//#include "myPhysics.hh"

#ifdef _WIN32
// @todo: handling Ctrl+C, e.g. http://msdn.microsoft.com/en-us/library/ms685049%28VS.85%29.aspx

#endif
#if defined (WIN32)
#include <windows.h>
#else
#include <signal.h>
#endif


#ifdef G4VIS_USE
 #include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include <ctime>

#define USE_STEPPING_COUNTER 1

namespace {
  G4RunManager *runManagerTop;
  void PrintUsage() {
    G4cerr << " Usage: " << G4endl;
    G4cerr << " Test04 [-re <random engine>] [--help] [-m <macros>] [-u] [-ph <usePhysics>] [--nelp <neutron elastic physics>]" << G4endl;
    G4cerr << "        [--nielp <neutron inelastic physics>] [--ncp <neutron capture physics>] [--emp <em physics>]" << G4endl;
    G4cerr << "        [-cont <continue>] [--noBinaryDump <0|1>] [--binaryFileName <filename for binary dump>]" << G4endl;
    G4cerr << " /?, --help - This help " << G4endl;
    G4cerr << " [-re <random engine>] Choose random engine" << G4endl;
    G4cerr << "                  Available <random engine>: " << G4endl;
    G4cerr << "                  ranecu " << G4endl;
    G4cerr << "                  ranlux64 " << G4endl;
    G4cerr << "                  mtwist " << G4endl;
    G4cerr << " [--nelp <neutron elastic physics>] Choose neutron elastic physics" << G4endl;
    G4cerr << "                  default - chips, XS chips" << G4endl;
    G4cerr << "                  chips - chips, XS chips" << G4endl;
    G4cerr << "                  chipswn - chips, XS G4NeutronElasticXS" << G4endl;
    G4cerr << "                  hadr - G4HadronElastic, XS G4NeutronElasticXS" << G4endl;
    G4cerr << "                  hp - HP, XS HP" << G4endl;
    G4cerr << "                  hpthermal - HP+thermal, XS HP" << G4endl;
    G4cerr << "                  lend - LEND, XS LEND (not working now)" << G4endl;
    G4cerr << " [--nielp <neutron inelastic physics>] Choose neutron inelastic physics" << G4endl;
    G4cerr << "                  default - Bertini cascade" << G4endl;
    G4cerr << "                  bert - Bertini cascase" << G4endl;
    G4cerr << "                  bin - Binary cascade" << G4endl;
    G4cerr << "                  nxl - INCLXX" << G4endl;
    G4cerr << "                  nxlp2 - PRECO < 2 MeV, INCLXX > 2 MeV" << G4endl;
    G4cerr << "                  nxlp2a - PRECO < 2 MeV, INCLXX > 1 MeV" << G4endl;
    G4cerr << "                  nxlp5 - PRECO < 5 MeV, INCLXX > 5 MeV" << G4endl;
    G4cerr << "                  nxlp5a - PRECO < 5 MeV, INCLXX > 4 MeV" << G4endl;
    G4cerr << "                  nxl5bert - INCLXX < 5 MeV, Bertini cascade > 5 MeV" << G4endl;
    G4cerr << "                  nxl10bert - INCLXX < 10 MeV, Bertini cascade > 10 MeV" << G4endl;
    G4cerr << "                  nxl5bin - INCLXX < 5 MeV, Binary cascade > 5 MeV" << G4endl;
    G4cerr << "                  nxl10bin - INCLXX < 10 MeV, Binary cascade > 10 MeV" << G4endl;
    G4cerr << "                  precobert - PRECO < 5 MeV, Bertini cascade > 5 MeV" << G4endl;
    G4cerr << "                  precobin / precobin2 - PRECO < 5 MeV, Binary cascade > 5 MeV" << G4endl;
    G4cerr << "                  hp - HP" << G4endl;
    G4cerr << "                  ie3N - PRECO < 5 MeV, 5 MeV < Binary cascade < 10 MeV, INCLXX > 10 MeV" << G4endl;
    G4cerr << "                  ie3 - PRECO < 5 MeV, 5 MeV < Binary cascade < 10 MeV, Bertini cascade > 10 MeV" << G4endl;
    G4cerr << "                  bin5bert - Binary cascade < 5 MeV, Bertini cascade > 5 MeV" << G4endl;
    G4cerr << "                  bin10bert - Binary cascade < 10 MeV, Bertini cascade > 10 MeV" << G4endl;
    G4cerr << "                  bin5nxl - Binary cascade < 5 MeV, INCLXX > 5 MeV" << G4endl;
    G4cerr << "                  bin10nxl - Binary cascade < 10 MeV, INCLXX > 10 MeV" << G4endl;
    G4cerr << " [--ncp <neutron capture physics>] Choose neutron capture physics" << G4endl;
    G4cerr << "                  hp - HP" << G4endl;
    G4cerr << "                  any - G4RadCapture" << G4endl;
    G4cerr << " [--emp <em physics>] Electromagnetic physics" << G4endl;
    G4cerr << "                  <empty_string>, em, em3 - G4EmStandardPhysics_option3" << G4endl;
    G4cerr << "                  em0 - G4EmStandardPhysics" << G4endl;
    G4cerr << "                  em1 - G4EmStandardPhysics_option1" << G4endl;
    G4cerr << "                  em2 - G4EmStandardPhysics_option2" << G4endl;
    G4cerr << "                  em4 - G4EmStandardPhysics_option4" << G4endl;
    G4cerr << "                  liver - G4EmLivermorePhysics" << G4endl;
    G4cerr << "                  penel - G4EmPenelopePhysics" << G4endl;
    G4cerr << "                  emlow - G4EmLowEPPhysics" << G4endl;
    G4cerr << "                  " << G4endl;
    G4cerr << " [-m <macros file>] Choose macros file" << G4endl;
    G4cerr << " [-u] use UI manager for graphics" << G4endl;
    G4cerr << " [-cont <continue>] 1 - to read binary dump and start from it (continue previous run)" << G4endl;
    G4cerr << G4endl;
    G4cerr << "Note: --nelp, --nielp, --ncp, --emp affects only if \"-ph 1\" set." << G4endl;
  }
  void makeClearRun() {
    // @todo: write deleting all files
  }
#if defined (WIN32)
  BOOL CtrlHandler( DWORD fdwCtrlType ) 
{ 
  G4String a;
  G4double pt;
  G4int pn;
  switch( fdwCtrlType ) 
  { 
    case CTRL_C_EVENT: 
      printf( "Ctrl-C event got\n\n" );
      //runManagerTop->AbortRun();
      //((RunAction*)runManagerTop->GetUserRunAction())->checkHasBestSolution();
      //runManagerTop->GetUserRunAction()

      //G4cin>>a;
      //G4cout<<"Got :"<<a;
      //((RunAction*)runManagerTop->GetUserRunAction())->dumpAllRA();
      //((RunAction*)runManagerTop->GetUserRunAction())->dumpDetCf();
      //((RunAction*)runManagerTop->GetUserRunAction())->dumpDetData();
      //((RunAction*)runManagerTop->GetUserRunAction())->dumpBinaryData();
      //((RunAction*)runManagerTop->GetUserRunAction())->printDose();
      //((RunAction*)runManagerTop->GetUserRunAction())->dumpRootData();
      //((RunAction*)runManagerTop->GetUserRunAction())->closeRootData();
      //((RunAction*)runManagerTop->GetUserRunAction())->StoreDB(1);
      ((RunAction*)runManagerTop->GetUserRunAction())->myEndRun(1);
#ifdef USE_STEPPING_COUNTER
      //((RunAction*)runManagerTop->GetUserRunAction())->printProcessesStats();
      //((RunAction*)runManagerTop->GetUserRunAction())->dumpRootData();
#endif
      runManagerTop->AbortRun();
      return FALSE; 
      //return( TRUE );

    case CTRL_BREAK_EVENT: 
      printf( "Ctrl-Break event\n\n" );
      //pt = ((RunAction*)runManagerTop->GetUserRunAction())->getCurrentRunElapsed();
      //G4cout<<"Current run elapsed time: "<<pt<<" secs"<<G4endl;
      //pt = ((RunAction*)runManagerTop->GetUserRunAction())->getAllRunElapsed();
      //G4cout<<"All runs elapsed time: "<<pt<<" secs"<<G4endl;
      //((RunAction*)runManagerTop->GetUserRunAction())->printDetectorsError();
      //return FALSE; 
      //((RunAction*)runManagerTop->GetUserRunAction())->dumpDetData(false);
      //((RunAction*)runManagerTop->GetUserRunAction())->printStatistics();
      return( TRUE );

    default: 
      return FALSE; 
  } 
} 
#else
  void CtrlBreakBackend(int sig) {
    printf( "Ctrl-Break event\n\n" );
    pt = ((RunAction*)runManagerTop->GetUserRunAction())->getCurrentRunElapsed();
    G4cout<<"Current run elapsed time: "<<pt<<" secs"<<G4endl;
    ((RunAction*)runManagerTop->GetUserRunAction())->printDetectorsError();
  }
#endif

  void handleCtrlCEventInit(G4RunManager *runManagerMain) {
    runManagerTop = runManagerMain;
#if defined (WIN32)
    SetConsoleCtrlHandler( (PHANDLER_ROUTINE) CtrlHandler, TRUE );
#else
  #ifdef SIGBREAK
    signal(SIGBREAK, CtrlBreakBackend);
  #endif
#endif

  }
}

 
int main(int argc,char** argv) {
  clock_t beginAt = clock();
#ifdef G4MULTITHREADED
  G4int nThreads = 0;
#endif
  myParamReader &paramReader = myParamReader::GetInstance();
  paramReader.setVerbose(2);
  paramReader.setInput(argc, argv);
  //myParamReader *paramReader = myParamReader::GetInstance();
  //paramReader->setVerbose(2);
  //paramReader->setInput(argc, argv);
  //myParamReader::GetInstance().setInput

  G4String cmd;
  G4String randomEngine = "ranecu";
  G4bool useUi = false;
  G4bool usePhys = false;
  G4int standartPhysNum = -1;
  G4bool noBinaryDump = false;
  G4String binaryFileName = "output";

  CLHEP::Ranlux64Engine defaultEngine( 1234567, 4 ); 
  G4Random::setTheEngine( &defaultEngine ); 
  G4int seed = time( NULL ); 
  G4Random::setTheSeed( seed );   
  

#ifdef G4MULTITHREADED
  G4MTRunManager * runManager = new G4MTRunManager;
  if ( nThreads > 0 ) { 
    runManager->SetNumberOfThreads(nThreads);
  }  
#else
  G4RunManager * runManager = new G4RunManager;
#endif

  DetectorConstruction* fdet = new DetectorConstruction();
  runManager->SetUserInitialization(fdet);

  //G4VModularPhysicsList* physicsList;
  G4VUserPhysicsList* physicsList = 0;
  if (!paramReader.isUseBasePhysics()) {
    physicsList= new myPhysicsList;
  } else {
    standartPhysNum = paramReader.getPhysicsModifierValue();
    if (1 == standartPhysNum) {
      physicsList= new FTFP_BERT;
    } else if (2 == standartPhysNum) {
      physicsList = new QBBC;
    } else if (3 == standartPhysNum) {
      physicsList = new QGSP_BIC;
    } else if (4 == standartPhysNum) {
      physicsList = new QGSP_BIC_HP;
    } else if (5 == standartPhysNum) {
      physicsList = new FTFP_BERT_HP;
    } else if (6 == standartPhysNum) {
      physicsList = new QGSP_INCLXX;
    } else if (7 == standartPhysNum) {
      physicsList = new QGSP_INCLXX_HP;
    } else if (8 == standartPhysNum) {
      physicsList = new FTFP_INCLXX;
    } else if (9 == standartPhysNum) {
      physicsList = new FTFP_INCLXX_HP;
    } else if (24 == standartPhysNum) {
      physicsList == new Shielding;
    } else {
      physicsList= new FTFP_BERT;
    }
  }

  runManager->SetUserInitialization(physicsList);

  ActionInitialization* AI = new ActionInitialization(fdet);

  runManager->SetUserInitialization(AI);

  runManager->Initialize();

  G4cout<<"Vis_Use "<<G4VIS_USE<<G4endl;

#if defined (WIN32)
  handleCtrlCEventInit(runManager);
#endif

  G4UImanager* UImanager = G4UImanager::GetUIpointer();

#ifdef G4VIS_USE
  G4VisManager* visManager;
  //if (paramReader.isUseUI()) {  // @todo: check this case
    visManager = new G4VisExecutive;
    visManager->Initialize();
  //}
#endif

  if (paramReader.isUseUI()) {
#ifdef G4UI_USE
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);
  #ifdef G4VIS_USE
    UImanager->ApplyCommand("/control/execute "+paramReader.getCmd()); 
  #else
    // @todo: case if no vis use but -u presented at cmd
    UImanager->ApplyCommand("/control/execute init.mac"); 
  #endif
    ui->SessionStart();
    delete ui;
#endif
  } else  if ( paramReader.getCmd().size() ) {
    // batch mode
    G4String command = "/control/execute ";
    UImanager->ApplyCommand(command+paramReader.getCmd());
  }

#ifdef G4VIS_USE
  delete visManager;
#endif
  delete runManager;
  
  G4cout<<"Total time: "<<(clock() - beginAt) / CLOCKS_PER_SEC<<G4endl;
  return 0;
}
