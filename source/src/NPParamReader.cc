#include "NPParamReader.hh"
#include "G4String.hh"
#include "globals.hh"
#include "boost/lexical_cast.hpp"

void myParamReader::preInit() {
  /**
   * Initialize variables with default values
   */
  readComplete = false;
  randomEngine = "ranecu";
  useUI = false;
  isBasePhysics = false;
  basePhysicsCase = -1;
  myPhysicsModifier = -1;
  isTimeSeed = false;
  if (verboseLevel > 0) {
    G4cout<<"==== myParamReader parsing input ===="<<G4endl;
  }
}

void myParamReader::postInit() {
  readComplete = true;
  if (verboseLevel > 0) {
    G4cout<<"==== myParamReader finished parsing input ===="<<G4endl;
  }
}

G4int myParamReader::getPhysicsModifierValue() {
  if (isBasePhysics) {
    return basePhysicsCase;
  }
  return myPhysicsModifier;
}

G4int myParamReader::getPhysicsModifierValue(G4bool oldStyle) {
  if (oldStyle && isBasePhysics) {
    return -1*basePhysicsCase;
  }
  return getPhysicsModifierValue();
}

void myParamReader::setInput(int argc, char** argv) {
  if (readComplete) {
    G4cout<<"==== ERROR: myParamReader seems to read twice ===="<<G4endl;
    return;
  }
  preInit();
  for ( int i=1; i<argc; i=i+2 ) {
    if ( G4String(argv[i]) == "-m" ) {
      cmd = G4String(argv[i+1]);
      if (verboseLevel > 1) {
        G4cout<<"Parameter \"-m\" parsed to myParamReader::cmd with value \""<<cmd<<"\""<<G4endl;
      }
    } else if (G4String(argv[i]) == "-re") {
      randomEngine = G4String(argv[i+1]);
      if (verboseLevel > 1) {
         G4cout<<"Parameter \"-re\" parsed to myParamReader::randomEngine with value \""<<randomEngine<<"\""<<G4endl;
      }
    } else if (G4String(argv[i]) == "-u") {
      useUI = static_cast<G4bool>(std::atoi(argv[i+1]));
      if (verboseLevel > 1) {
        G4cout<<"Parameter \"-u\" parsed to myParamReader::useUI, value \""<<argv[i+1]<<"\" casted as \""<<useUI<<"\""<<G4endl;
      }
    } else if (G4String(argv[i]) == "-ph") {
      G4int phVal = static_cast<G4int>(std::atoi(argv[i+1]));
      if (phVal < 0) {
        isBasePhysics = true;
        basePhysicsCase = -1*phVal;
        if (verboseLevel > 1) {
          G4cout<<"Parameter \"-ph\" parsed to myParamReader::isBasePhysics as \"1\""
            <<" and myParamReader::basePhysicsCase casted as \""<<basePhysicsCase<<"\""<<G4endl;
        }
      } else {
        isBasePhysics = false;
        myPhysicsModifier = phVal;
          G4cout<<"Parameter \"-ph\" parsed to myParamReader::isBasePhysics as \"0\""
            <<" and myParamReader::myPhysicsModifier casted as \""<<myPhysicsModifier<<"\". Problem physics is used now."<<G4endl;
      }
    } else if (G4String(argv[i]) == "--seed") {
      if (G4String(argv[i+1]) == "time") {
        isTimeSeed = true;
        G4cout<<"Parameter \"--seed\" parsed to myParamReader::isTimeSeed as \"1\". Using time(NULL) as seed."<<G4endl;
      } else {
        isTimeSeed = false;
        //userSeed = static_cast<long>(std::atol(argv[i+1]));
        //userSeed = std::atol(argv[i+1]);
        userSeed = boost::lexical_cast<long long>(argv[i+1]);
        G4cout<<"Parameter \"--seed\" parsed to myParamReader::isTimeSeed as \"0\" and myParamReader::userSeed is \""<<userSeed<<"\" now."<<G4endl;
      }
    }
    
    else {
      G4String v1, v2;
      v1 = G4String(argv[i]);
      v2 = G4String(argv[i+1]);
      if (0 == otherParams.count(v1)) {
        otherParams.insert(std::pair<G4String, G4String>(v1, v2));
        if (verboseLevel > 1) {
          G4cout<<"Parameter \""<<v1<<"\" parsed as myParamReader::otherParams with value \""<<v2<<"\""<<G4endl;
        }
      } else {
        if (verboseLevel > 1) {
          G4cout<<"Parameter \""<<v1<<"\" duplicated in myParamReader::otherParams (new value \""<<v2<<"\")"<<G4endl;
        }
      }
    }
  }


  postInit();
}

G4String myParamReader::getOtherParamAsString(G4String pKey) {
  return getOtherParamAsString(pKey, "");
}
G4String myParamReader::getOtherParamAsString(G4String pKey, G4String def="") {
  if (0 != otherParams.count(pKey)) 
    return otherParams.find(pKey)->second;
  if (0 != otherParams.count(G4String("--")+pKey))
    return otherParams.find(G4String("--")+pKey)->second;
  return def;
}

/*static*/ NPParamReader2* NPParamReader2::NPParamInstance = 0;

#include "G4AutoLock.hh"
namespace {
  G4Mutex NPParamMutex = G4MUTEX_INITIALIZER;
}

NPParamReader2* NPParamReader2::Instance() {
  G4AutoLock l(&NPParamMutex);
  if ( !NPParamInstance ) NPParamInstance = new NPParamReader2();
  return NPParamInstance;
}

G4int NPParamReader2::getSomething() {
  return 4;
}