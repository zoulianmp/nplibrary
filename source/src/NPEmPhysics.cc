#include "NPEmPhysics.hh"

#include "G4SystemOfUnits.hh"
#include "G4ParticleDefinition.hh"
#include "G4EmParameters.hh"
#include "G4LossTableManager.hh"
#include "G4ProcessManager.hh"
#include "G4PhysicsListHelper.hh"
#include "G4BuilderType.hh"
#include "G4PhysicsConstructorFactory.hh"
#include "G4RegionStore.hh"

#include "G4ionIonisation.hh"

// angular generators
#include "G4Generator2BS.hh"
#include "G4Generator2BN.hh"
#include "G4ModifiedTsai.hh"

#include "G4IonParametrisedLossModel.hh"
#include "G4BetheBlochModel.hh"
#include "G4hBetheBlochModel.hh"
#include "G4BraggIonModel.hh"
#include "G4BraggModel.hh"
#include "G4BohrFluctuations.hh"
#include "G4UAtomicDeexcitation.hh"
#include "G4hMultipleScattering.hh"
#include "G4NuclearStopping.hh"
#include "G4CoulombScattering.hh"
#include "G4eMultipleScattering.hh"
#include "G4UrbanMscModel.hh"
#include "G4WentzelVIModel.hh"
#include "G4eCoulombScatteringModel.hh"
#include "G4eIonisation.hh"
#include "G4LivermoreIonisationModel.hh"
#include "G4eBremsstrahlung.hh"
#include "G4UniversalFluctuation.hh"
#include "G4LivermoreBremsstrahlungModel.hh"
#include "G4PenelopeBremsstrahlungModel.hh"
#include "G4SeltzerBergerModel.hh"
#include "G4hIonisation.hh"
#include "G4hBremsstrahlung.hh"
#include "G4hPairProduction.hh"
#include "G4PAIModel.hh"

#include "G4ComptonScattering.hh"
#include "G4KleinNishinaModel.hh"
#include "G4PhotoElectricEffect.hh"
#include "G4GammaConversion.hh"
#include "G4RayleighScattering.hh"

#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4Gamma.hh"
#include "G4GenericIon.hh"
#include "G4Proton.hh"

//
G4_DECLARE_PHYSCONSTR_FACTORY(NPEmPhysics);

NPEmPhysics::NPEmPhysics(G4int ver) 
  : G4VPhysicsConstructor("NPEmPhysics"), verbose(ver)
{
  G4EmParameters* param = G4EmParameters::Instance();
  param->SetVerbose(verbose);
  param->SetLatDisplacementBeyondSafety(true);
  param->SetMuHadLateralDisplacement(false);
  param->ActivateAngularGeneratorForIonisation(true);
  SetPhysicsType(bElectromagnetic);
}

NPEmPhysics::~NPEmPhysics()
{
}

void NPEmPhysics::ConstructParticle() {
  G4Electron::Electron();
  G4Electron::ElectronDefinition();
  G4Gamma::Gamma();
  G4Gamma::GammaDefinition();
  G4GenericIon::GenericIonDefinition();
}

void NPEmPhysics::ConstructProcess() {
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();

  //G4Region* gas = theRegionStore->GetRegion("VertexDetector");

  aParticleIterator->reset();
  while( (*aParticleIterator)() ) {
    G4ParticleDefinition* particle = aParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();

    if (particleName == "GenericIon") {
      //ph->RegisterProcess(hmsc, particle);
      //ph->RegisterProcess(new G4ionIonisation(), particle);
      G4ionIonisation* ionIoni = new G4ionIonisation();
      //G4IonParametrisedLossModel *ipm = new G4IonParametrisedLossModel();
      G4BraggIonModel *ipm = new G4BraggIonModel();
      ipm->SetHighEnergyLimit(2*MeV);
      G4BetheBlochModel *bpm = new G4BetheBlochModel();
      //G4BohrFluctuations *flu = new G4BohrFluctuations();
      G4UniversalFluctuation *flu = new G4UniversalFluctuation();
      bpm->SetLowEnergyLimit(2*MeV);
      ionIoni->AddEmModel(0, ipm, flu);
      ionIoni->AddEmModel(0, bpm, flu);
      //ionIoni->SetEmModel(bpm);
      //ionIoni->AddEmModel(0, ipm);
      //ionIoni->SetEmModel(new G4IonParametrisedLossModel());

      ionIoni->SetStepFunction(0.1, 1*um);
      //G4CoulombScattering* cs = new G4CoulombScattering();
      //cs->SetBuildTableFlag(false);
      ph->RegisterProcess(ionIoni, particle);
      //ph->RegisterProcess(cs, particle);
      G4hMultipleScattering *ionmsc = new G4hMultipleScattering("ionmsc");
      ph->RegisterProcess(ionmsc, particle);
      ph->RegisterProcess(new G4NuclearStopping(), particle);
      //pmanager->AddProcess(ionIoni,       -1,-1, 1);
    }
    else if (particleName == "e-") {
      // multiple scattering
      G4eMultipleScattering* msc = new G4eMultipleScattering("emsc");
      //msc->SetStepLimitType(fUseDistanceToBoundary);
      msc->SetStepLimitType(fMinimal);
      G4UrbanMscModel* msc1 = new G4UrbanMscModel();
      G4WentzelVIModel* msc2 = new G4WentzelVIModel();
      G4double highEnergyLimit = 100*MeV;
      msc1->SetHighEnergyLimit(highEnergyLimit);
      msc2->SetLowEnergyLimit(highEnergyLimit);
      msc->SetRangeFactor(0.01);
      msc->AddEmModel(0, msc1);
      msc->AddEmModel(0, msc2);

      G4eCoulombScatteringModel* ssm = new G4eCoulombScatteringModel(); 
      G4CoulombScattering* ss = new G4CoulombScattering("ec");
      ss->SetEmModel(ssm, 1); 
      ss->SetMinKinEnergy(highEnergyLimit);
      ssm->SetLowEnergyLimit(highEnergyLimit);
      ssm->SetActivationLowEnergyLimit(highEnergyLimit);
      
      // Ionisation - Livermore should be used only for low energies
      G4eIonisation* eIoni = new G4eIonisation("eIoni");
      G4LivermoreIonisationModel* theIoniLivermore = new
        G4LivermoreIonisationModel();
      theIoniLivermore->SetHighEnergyLimit(0.1*MeV); 
      //const G4RegionStore* theRegionStore = G4RegionStore::GetInstance();
      //G4Region* regFilter = theRegionStore->GetRegion("regFilter");
      eIoni->AddEmModel(0, theIoniLivermore, new G4UniversalFluctuation()/*, regFilter*/ );
      //G4PAIModel*     pai = new G4PAIModel(particle,"PAIModel");
      //eIoni->AddEmModel(0, pai);
      //eIoni->SetEmModel(pai);
      //G4EmConfigurator *fConfig = G4LossTableManager::Instance()->EmConfigurator();
      //pai->SetLowEnergyLimit(0.1*MeV);
      //fConfig->SetExtraEmModel(pai);

      eIoni->SetStepFunction(0.2, 1*um); //     
      
      // Bremsstrahlung
      G4eBremsstrahlung* eBrem = new G4eBremsstrahlung("eBrem");
      G4VEmModel* theBremLivermore = new G4LivermoreBremsstrahlungModel();
      G4PenelopeBremsstrahlungModel* theBremPenelope = new G4PenelopeBremsstrahlungModel();
      G4SeltzerBergerModel* br1 = new G4SeltzerBergerModel();
      //G4eBremsstrahlungRelModel* br2 = new G4eBremsstrahlungRelModel();
      //theBremPenelope->SetHighEnergyLimit(1*GeV);
      //theBremPenelope->SetLowEnergyLimit(500*keV);
      //theBremPenelope->SetAngularDistribution(new G4ModifiedTsai());
      //theBremLivermore->SetHighEnergyLimit(500*keV);
      //theBremLivermore->SetAngularDistribution(new G4Generator2BN());
      //br2->SetLowEnergyLimit(1*GeV);
      //br2->SetAngularDistribution(new G4Generator2BS());
      //eBrem->SetEmModel(theBremLivermore,1);
      //eBrem->AddEmModel(0,theBremLivermore);
      eBrem->AddEmModel(0,theBremPenelope);
      //br1->SetAngularDistribution(new G4Generator2BN());
      //eBrem->AddEmModel(0,br1);
      //eBrem->SetEmModel(br1, 1);
      eBrem->SetStepFunction(0.1, 1*um);
     
      // register processes
      //ph->RegisterProcess(msc, particle);
      ph->RegisterProcess(eIoni, particle);
      ph->RegisterProcess(eBrem, particle);
      //ph->RegisterProcess(ss, particle);
    } 
    else if (particleName == "gamma") {
      G4ComptonScattering* cs = new G4ComptonScattering;
      cs->SetEmModel(new G4KleinNishinaModel());

      ph->RegisterProcess(new G4PhotoElectricEffect(), particle);
      ph->RegisterProcess(cs, particle);
      ph->RegisterProcess(new G4GammaConversion(), particle);
      ph->RegisterProcess(new G4RayleighScattering(), particle);
    }
    //else if (particleName == "proton") {
    //  G4hIonisation* pIoni = new G4hIonisation("pIoni");
    //  //pIoni->SetDEDXBinning()
    //  pIoni->SetStepFunction(0.05, 1*um);
    //  ph->RegisterProcess(pIoni, particle);
  
    //  /*G4ScreenedNuclearRecoil* nucr = new G4ScreenedNuclearRecoil();
    //  G4double energyLimit = 100.*MeV;
    //  nucr->SetMaxEnergyForScattering(energyLimit);*/
    //  ph->RegisterProcess(new G4hMultipleScattering("pMsc"), particle);
    //  ph->RegisterProcess(new G4hBremsstrahlung("pBrem"), particle);
    //  ph->RegisterProcess(new G4hPairProduction("pPair"), particle);
    //}
  }

  G4VAtomDeexcitation* de = new G4UAtomicDeexcitation();
  de->SetFluo(true);
  de->SetAuger(true);  
  de->SetPIXE(true);  
  G4LossTableManager::Instance()->SetAtomDeexcitation(de);
}