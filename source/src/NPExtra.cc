#include "NPExtra.hh"
#include "globals.hh"

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <windows.h>
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

mDBSingleton* mDBSingleton::instance = 0;

#include "G4AutoLock.hh"
namespace {
  G4Mutex mDBSingletonMutex = G4MUTEX_INITIALIZER;
};

mDBSingleton* mDBSingleton::GetInstance()
{
  G4AutoLock l(&mDBSingletonMutex);
  if ( !instance ) instance = new mDBSingleton();
  return instance;
}

void mDBSingleton::addlt(G4int a, G4double b) {
  if (ltT.count(a) == 0) {
    ltT.insert(std::pair<G4int, G4double>(a, b));
  }
}

G4double mDBSingleton::getlt(G4int a) {
  if (ltT.count(a) == 0) {
    return -1;
  }
  return ltT.find(a)->second;
}

void mDBSingleton::addMetaData(G4String key, G4int value) {
  if (string2IntMetaData.count(key) == 0) {
    string2IntMetaData.insert(std::pair<G4String, G4int>(key, value));
  }
}

G4int mDBSingleton::getS2IMeta(G4String key) {
  if (string2IntMetaData.count(key) == 0) {
    return n_NOT_FOUND;
  }
  return string2IntMetaData.find(key)->second;
}

namespace NPExtra {
  void __GetMachineName(char* machineName)
  {
      char Name[150];
      int i=0;

      #ifdef WIN32
          TCHAR infoBuf[150];
          DWORD bufCharCount = 150;
          memset(Name, 0, 150);
          if( GetComputerName( infoBuf, &bufCharCount ) )
          {
              for(i=0; i<150; i++)
              {
                  Name[i] = infoBuf[i];
              }
          }
          else
          {
              strcpy(Name, "Unknown_Host_Name");
          }
      #else
          memset(Name, 0, 150);
          gethostname(Name, 150);
      #endif
      strncpy(machineName,Name, 150);
  }
}