#ifndef layerData_h
#define layerData_h 1

#include "globals.hh"

#include <boost/archive/binary_oarchive.hpp> 
#include <boost/archive/binary_iarchive.hpp> 
#include <boost/serialization/map.hpp> 
#include <boost/serialization/string.hpp> 
#include <boost/serialization/list.hpp> 
#include <boost/serialization/vector.hpp>
#include <boost/serialization/level_enum.hpp>
#include <boost/serialization/collection_traits.hpp>
#include <boost/serialization/is_bitwise_serializable.hpp>

#include <map>
#include <vector>
#include "G4ThreeVector.hh"
#include "NPExtra.hh"

#ifdef G4MULTITHREADED
#include "G4Threading.hh"
#include "G4AutoLock.hh"
#endif

G4int processNameToNum(G4String procName);

struct fluenceData {
  // @todo: currently unused
  G4int noP; // number of particles got
  G4int noIonPrim; // number of ionisation acts from primaries
  G4int noIonAll; // number of ionisation acts from all particles
  //G4ParticleDefinition l;
  // @todo: add particle number per type
};

struct voxelMeta {
  G4double mass;
  G4double density;
  G4double electronDensity;

};

struct runDataLayer {
  G4double depEnergy, depEnergy2;  // ����������� �������, �������� ����������� �������, ���
  G4double nEvents, nEventsNonZero; // ����� ������, ����� ������ � ��������� ���������
  G4double letBase, letBase2;
  G4double cellFlux, cellFlux2;
  void Nullify();
  runDataLayer();
  void Update(G4double, G4double, G4double);
  //runDataLayer(G4double, G4int, G4int, G4double);
  //runDataLayer(G4double, G4int, G4int, G4double, G4double);
  runDataLayer(G4double _dE, G4double _dE2, G4int _nE, G4int _nZ, G4double _let, G4double _let2, G4double _cf, G4double _cf2);
  runDataLayer(G4double, G4double, G4double);
  runDataLayer operator+ (const runDataLayer &other);
  runDataLayer& operator+= (const runDataLayer &other);

private: 
    friend class boost::serialization::access; 
    template <typename Archive> void serialize(Archive &ar, const unsigned int version) {
        ar & depEnergy; 
        ar & depEnergy2; 
        ar & nEvents;
        ar & nEventsNonZero;
        ar & cellFlux;
        ar & cellFlux2;
    };
};

struct detLayer
{
  // @todo: for backward compability, remove this class at next release
	G4double depEnergy, depEnergy2, dose, dose2;
	G4int nEvents;
  G4double cellFlux, cellFlux2;
  detLayer();
  detLayer(G4double, G4double, G4double, G4double, G4int, G4double, G4double);
  void Nullify();
  bool Update(G4double, G4double, G4int, G4double);
  bool Update(G4double, G4double, G4double);
};

struct letLayer {
  // @todo: for backward compability, remove this class at next release
  G4double totalLet, totalLet2;
  G4double dTotalLet, dTotalLet2;
  letLayer();
  letLayer(G4double, G4double);
  void Nullify();
  bool Update(G4double, G4double);
  letLayer operator+ (const letLayer &other);
  letLayer& operator+= (const letLayer &other);
};

struct detVoxLayer {
  // @todo: for backward compability, remove this class at next release
  G4double dE, dE2;
  G4double dXSum, dXSum2;
  G4double eKin, eKin2;
  G4double dEdX, dEdX2;
  G4int nE;
  inline void Nullify() { dE = dE2 = dXSum = dXSum2 = eKin = eKin2 = dEdX = dEdX2 = 0.0; nE = 0; }
};

struct detLayerStepWeighted
{
  // @todo: for backward compability, remove this class at next release
  G4double eKins, eKins2;
  G4double eKinsNw, eKinsNw2;
  G4double eDep, eDep2;
  G4double sLength, sLength2;
  G4int nEvents;
  detLayerStepWeighted();
  detLayerStepWeighted(G4double, G4double, G4double, G4double, G4double, G4double, G4int);
  void Nullify();
  bool Update(G4double, G4double, G4double);
  detLayerStepWeighted operator+ (const detLayerStepWeighted &other);
  detLayerStepWeighted& operator+=(const detLayerStepWeighted &other);
};

struct resLayer
{
  // @todo: for backward compability, remove this class at next release
  G4double depEnergy, depEnergy2, dose, dose2;
  G4double depEnergyError, doseError;
	G4int nEvents;
  G4double cellFlux, cellFlux2;
  G4double cellFluxError;
  G4bool _errorState;
  resLayer();
  resLayer(detLayer);
  resLayer(G4double, G4double, G4double, G4double, G4int, G4double, G4double);
  resLayer(G4double, G4double, G4double, G4double, G4double, G4double, G4int, G4double, G4double, G4double);
  void Nullify();
  void calculateError();
  resLayer operator+ (const resLayer &other);
  resLayer operator+ (const detLayer &other);
  resLayer& operator+=(const resLayer &other);
  resLayer& operator+=(const detLayer &other);
  void save();
private: 
    friend class boost::serialization::access; 
    template <typename Archive> void serialize(Archive &ar, const unsigned int version) {
        ar & depEnergy; 
  ar & depEnergy2; 
  ar & nEvents;
  ar & dose;
  ar & dose2;
  ar & cellFlux;
  ar & cellFlux2;
    };
};


struct expFullData {
  //runDataLayer depData;
  //std::vector<G4int, runDataLayer> depData;
  //std::vector<G4int, fluenceData> fluenceData;
  //std::vector<G4int, voxelMeta> voxelsData;  @todo: <G4int, G4int>, 2nd int to complete data
  runDataLayer depData;
  fluenceData fData;
private:
  friend class boost::serialization::access; 
  template <typename Archive> void serialize(Archive &ar, const unsigned int version) {
    ar & depData;
    ar & fData;
  };
};


struct filterType {
  //G4bool used;
  enum usecasetype { 
    npF_gtKinetic,  // kinetic energy greater than filtered value
    npF_lwKinetic,  // kinetic energy lower than filtered value
    npF_gtDeDx,     // dE/dX greater than filtered value
    npF_lwDeDx,     // dE/dX lower than filtered value
    npF_procType,   // process type/subtype filter
    //npF_voxelTarget,// voxel num target @todo: implement slice
    npF_unused      // filter unused
  } usecase;
  union /*criteriaUnion*/ {
    G4double kineticCriteria;
    G4double deDxCriteria;
    G4int processTypeCriteria;
    // G4int *voxelOfInterest;
    //private:
    //  friend class boost::serialization::access; 
    //  template <typename Archive> void serialize(Archive &ar, const unsigned int version) {
    //    ar & kineticCriteria;
    //    ar & deDxCriteria;
    //    ar & processTypeCriteria;
    //  };
  } criteria;
  bool allowed(G4double, G4double, G4int);
  G4double getActiveCriteria();
  G4String getActiveCriteriaString();
  void setCriteria(G4double);

private:
  friend class boost::serialization::access; 
  template <typename Archive> void serialize(Archive &ar, const unsigned int version) {
    ar & usecase;
    ar & criteria;
  };
};
//BOOST_IS_BITWISE_SERIALIZABLE(filterType::criteriaUnion);
//BOOST_IS_BITWISE_SERIALIZABLE(filterType);

// filterType ft {filterType::f_TopKinetic, ...} ? // check auto implementation

typedef std::pair<filterType, runDataLayer> filteredRunDataLayer;
typedef std::pair<filterType, fluenceData> filteredFluenceData;
typedef std::map<G4int, runDataLayer> voxeledRunDataLayer;

class vxMain {
  //@todo: implement many detectors inside (now - use inheritance+own instance control)
  public:
#ifdef G4MULTITHREADED
    vxMain(bool isMaster=true);
#else
    vxMain();
#endif
    ~vxMain();

    static vxMain* Instance();
    void Init();

    void setVoxelData(G4int, G4int, G4int, G4ThreeVector);
    G4int getXCoordFromNum(G4int);
    G4int getYCoordFromNum(G4int);
    G4int getZCoordFromNum(G4int);
    G4int getNumFromXYZCoord(G4int, G4int, G4int);

    //void addFilter(G4double gtKinetic=0, G4double lwKinetic=0, G4double gtDeDx=0, G4double lwDeDx=0, G4int procType=0);
    void addFilter(G4double, filterType::usecasetype);
    void addData(G4int voxNum, G4double dE, G4double dX, G4double eKin, G4int processType);
    void addData(G4int vX, G4int vY, G4int vZ, G4double dE, G4double dX, G4double eKin, G4int processType) {addData(getNumFromXYZCoord(vX, vY, vZ), dE, dX, eKin, processType);};

    void printData(); // @todo: specify filter target

    void dump(G4String);
    void readDump(G4String);

    void finalize();

    G4double getAbsError(G4double, G4double, G4int);
    G4String getRelErrorString(G4double, G4double);
    
  private:

#ifdef G4MULTITHREADED
    static vxMain* vxMasterInstance;
    static G4ThreadLocal vxMain* vxInstance;    
    void merge(G4int rdNum, std::map<G4int, voxeledRunDataLayer> rdTarget);
#else
    static vxMain* vxInstance;
#endif

    std::vector<G4int> affectedVoxelNums;

    bool isMetaSet;
    G4int metaQX;
    G4int metaQY;
    G4int metaQZ;
    //G4LogicalVolume *voxLV;
    G4ThreeVector metaVxSize; // probably no save for serialization


    // map <voxel_num, data> for different case
    std::map<G4int, filterType> fdData;
    std::map<G4int, voxeledRunDataLayer> _rdAll; // <filterNum, <voxelNum, runDataLayer>>
    // _fd0 unused now
    std::map<G4int, filteredFluenceData> _fd0;
    std::map<G4int, filteredFluenceData> _fd1;
    std::map<G4int, filteredFluenceData> _fd2;
    std::map<G4int, filteredFluenceData> _fd3;
    std::map<G4int, filteredFluenceData> _fd4;
    G4int filterUsedCounter;

    void _addFilterToMap(filterType);
    void _addFilterToMap(filterType, int);
    void _addDataToRD(G4int, G4int, G4double, G4double);

    //std::
private:
  friend class boost::serialization::access; 
  template <typename Archive> void serialize(Archive &ar, const unsigned int version) {
    ar & affectedVoxelNums;
    ar & metaQX;
    ar & metaQY;
    ar & metaQZ;
    ar & filterUsedCounter;
    //ar & _rd0;
    //ar & _rd1;
    //ar & _rd2;
    //ar & _rd3;
    //ar & _rd4;
    ar & _rdAll;
    //ar & fdData;
  };
};

#endif