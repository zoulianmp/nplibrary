#ifndef NPPhysicsReader_hh
#define NPPhysicsReader_hh
#include <boost/config/compiler/visualc.hpp>
//#include <boost/config/compiler/gcc.hpp>  
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <iostream>
#include <ios>
#include <string>
#include <vector>
#include <map>
#include "globals.hh"
#include "G4ParticleDefinition.hh"
#include "NPHadrPhysics.hh"

namespace NPPhysicsReader {
  //G4String fName = "C:\\Temp\\geantProjects\\OldButActual\\Proton1-8MeVEBT_build\\Release\\myPhysics.json";
  std::vector<std::string> getListParticles(std::string);
  void setModel(std::string, G4ParticleDefinition*, G4ProcessManager*, NPHadrPhysics*);
  //struct modelData {
  //  G4String modelName;
  //  G4double highLimit;
  //  G4double lowLimit;
  //};

  //typedef std::map<G4String, modelData> intMdData;

  //std::map<G4String, intMdData> particle2Model;
};
#endif