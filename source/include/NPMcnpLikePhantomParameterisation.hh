#ifndef NPMcnpLikePhantomParameterisation_hh
#define NPMcnpLikePhantomParameterisation_hh

#include <vector>
#include <map>

#include "G4Types.hh"
#include "G4ThreeVector.hh"
//#include "G4VPVParameterisation.hh"
#include "G4VNestedParameterisation.hh" 

class G4VPhysicalVolume;
class G4VTouchable; 
class G4VSolid;
class G4Material;
class G4Element;
class G4VisAttributes;

// CSG Entities which may be parameterised/replicated
//
class G4Box;
class G4Tubs;
class G4Trd;
class G4Trap;
class G4Cons;
class G4Sphere;
class G4Ellipsoid;
class G4Orb;
class G4Torus;
class G4Para;
class G4Polycone;
class G4Polyhedra;
class G4Hype;

/// Implements a G4VNestedParameterisation

class NPMcnpLikePhantomParameterisation : public G4VNestedParameterisation
{
  public:

    NPMcnpLikePhantomParameterisation(const G4ThreeVector& voxelSize,
                                       G4int fnZ_ = 0, G4int fnY_ = 0, G4int fnX_ = 0);
   ~NPMcnpLikePhantomParameterisation(); 

    void ComputeTransformation(const G4int no,
                                     G4VPhysicalVolume *currentPV) const;

    G4Material* ComputeMaterial(G4VPhysicalVolume *currentVol,
                                const G4int repNo, 
                                const G4VTouchable *parentTouch );
      // Must cope with parentTouch for navigator's SetupHierarchy
  private:  // Dummy declarations to get rid of warnings ...

    void ComputeDimensions (G4Trd&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Trap&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Cons&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Sphere&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Orb&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Torus&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Para&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Hype&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Tubs&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Polycone&,const G4int,const G4VPhysicalVolume*) const {}
    void ComputeDimensions (G4Polyhedra&,const G4int,const G4VPhysicalVolume*) const {}

    G4int       GetNumberOfMaterials() const;
    G4Material* GetMaterial(G4int idx) const;

    void createMaterials();
   
  private:
    G4int matListCount;
    G4int *matListInts;
    G4double fdX,fdY,fdZ;
    G4int fnX,fnY,fnZ;
    std::map<G4int, G4double> materialDensities;
    std::map<G4int, G4Material*> materialData;
    std::map<G4int, G4Material*> materialData2;
    std::map<G4int, G4Element*> elementsData;
    G4Material **raw;

    //G4Material **loM;
    //G4int noM;
};
#endif
