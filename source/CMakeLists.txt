#----------------------------------------------------------------------------
# Setup the project
cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
project(NPLibrary)
include (GenerateExportHeader)

#----------------------------------------------------------------------------
# Find Geant4 package, activating all available UI and Vis drivers by default
# You can set WITH_GEANT4_UIVIS to OFF via the command line or ccmake/cmake-gui
# to build a batch mode only executable
#
option(WITH_GEANT4_UIVIS "Build example with Geant4 UI and Vis drivers" ON)
if(WITH_GEANT4_UIVIS)
  find_package(Geant4 REQUIRED ui_all vis_all)
else()
  find_package(Geant4 REQUIRED)
endif()
FIND_PACKAGE( Boost 1.52 REQUIRED )

set(CMAKE_MODULE_PATH
    ${PROJECT_SOURCE_DIR}/modules
    ${CMAKE_MODULE_PATH})

include(FindMongoDB)

#----------------------------------------------------------------------------
# Setup Geant4 include directories and compile definitions
#
include(${Geant4_USE_FILE})

#----------------------------------------------------------------------------
# Locate sources and headers for this project
#
include_directories(${PROJECT_SOURCE_DIR}/include 
                    ${Geant4_INCLUDE_DIR}
                    ${Boost_INCLUDE_DIR}
                    ${MongoDB_INCLUDE_DIR}
)
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cc)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.hh)

#----------------------------------------------------------------------------
# Add the executable, and link it to the Geant4 libraries
#
add_library(NPLibrary ${sources} ${headers})
GENERATE_EXPORT_HEADER( NPLibrary
             BASE_NAME NPLibrary
             EXPORT_MACRO_NAME NPLibrary_EXPORT
             EXPORT_FILE_NAME NPLibrary_Export.h
             STATIC_DEFINE NPLibrary_BUILT_AS_STATIC
)
target_link_libraries(NPLibrary 
 ${Geant4_LIBRARIES} 
 ${Boost_LIBRARIES}
 ${MongoDB_LIBRARIES}
)
link_directories (${Boost_LIBRARY_DIRS})
link_directories (${MongoDB_LIBRARIES})
LINK_DIRECTORIES(${MONGOCXX_LINK_DIR})

#----------------------------------------------------------------------------
# Copy all scripts to the build directory, i.e. the directory in which we
# build Hadr03. This is so that we can run the executable directly because it
# relies on these scripts being in the current working directory.


